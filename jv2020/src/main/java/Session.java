import java.time.LocalDate;
import java.time.LocalTime;

public class Session {
	
	public static final String[] stateSession = {"ACTIVE", "CLOSED"};
	
	private User user;
	private LocalDate date;
	private LocalTime startTime;
	private LocalTime endTime;
	private String state;
	
	public Session(User user) {
		assert user != null;
		
		this.setUser(user);
		this.date = LocalDate.now();
		this.startTime = LocalTime.now();
		this.endTime = LocalTime.now();
		this.state = stateSession[0];
	}

	public Session() {
		this(new User());
	}
	
	public Session(Session session) {
		assert session != null;
		
		this.user = new User(session.user);
		this.date = LocalDate.of(session.date.getDayOfYear(), 
				session.date.getMonth(), 
				session.date.getDayOfMonth()
				);
		this.startTime = LocalTime.of(session.startTime.getHour(), 
				session.startTime.getMinute(), 
				session.startTime.getSecond()
				);
		this.endTime = LocalTime.of(session.endTime.getHour(), 
				session.endTime.getMinute(), 
				session.endTime.getSecond()
				);
		this.state = new String(session.state);
	}
	
	public User getUser() {
		return this.user;
	}
	
	public LocalDate getDate() {
		return this.date;
	}
	
	public LocalTime getStartTime() {
		return this.startTime;
	}
	
	public LocalTime getEndTime() {
		return this.endTime;
	}
	
	public String getState() {
		return this.state;
	}
	
	public void setUser(User user) {
		assert user != null;
		this.user = user;
	}
	
	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}
	
	@Override
	public Session clone() {
		return  new Session(this);
	}
		
	@Override
	public String toString() {
		return String.format(
				"%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n",
						"user:", this.user.getName(), 
						"date:", this.date, 
						"startTime:", this.startTime, 
						"endTime:", this.endTime, 
						"state:", this.state	
				);
	}
}
