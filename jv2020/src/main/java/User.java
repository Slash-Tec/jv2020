
/**
 * Proyecto: Juego de la vida.
 */

import java.time.LocalDate;

public class User {

	public static final String[] roleUser = { "GUEST", "REGISTERED", "ADMIN" };
	
	private String nif;
	private String name;
	private String surnames;
	private String address;
	private String mail;
	private LocalDate birthDate;
	private LocalDate registeredDate;
	private String password;
	private String role;

	public User(String nif, String name, String surnames,
			String address, String mail, LocalDate birthDate,
			LocalDate registeredDate, String password, String role) {
		this.setNif(nif);
		this.setName(name);
		this.setSurnames(surnames);
		this.setAddress(address);
		this.setMail(mail);
		this.setBirthDate(birthDate);
		this.setRegisteredDate(registeredDate);
		this.setPassword(password);
		this.setRole(role);
	}

	public User() {
		this.nif = new String();
		this.name = new String();
		this.surnames = new String();
		this.address = new String();
		this.mail = new String();
		this.birthDate = LocalDate.now();
		this.registeredDate =  LocalDate.now();
		this.password = new String("Miau#00");
		this.role = User.roleUser[0];
	}

	public User(User user) {
		assert user != null;

		this.nif = new String(user.nif);
		this.name = new String(user.name);
		this.surnames = new String(user.surnames);
		this.address = new String(user.address);
		this.mail = new String(user.mail);	
		this.birthDate = LocalDate.of(user.birthDate.getYear(), 
				user.birthDate.getMonth(), 
				user.birthDate.getDayOfMonth()
				);	
		this.registeredDate = LocalDate.of(user.registeredDate.getYear(), 
				user.registeredDate.getMonth(), 
				user.registeredDate.getDayOfMonth()
				);
		this.password = new String(user.password);
		this.role = new String(user.role);	
	}

	public String getNif() {
		return nif;
	}

	public String getName() {

		return name;
	}

	public String getSurnames() {
		return surnames;
	}

	public String getAddress() {
		return address;
	}

	public String getMail() {
		return mail;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public LocalDate getRegisteredDate() {
		return registeredDate;
	}

	public String getPassword() {
		return password;
	}

	public String getRole() {
		return role;
	}

	public void setNif(String nif) {	
		assert nif != null;

		if (isValidNif(nif)) {
			this.nif = nif;
		}
	}

	private boolean isValidNif(String nif) {
	       return !nif.matches("[ ]+");        // Que no sea en blanco. Regex: [ ]+
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setBirthDate(LocalDate birthDate) {
		assert birthDate != null;

		if (isValidBirthDate(birthDate)) {
			this.birthDate = birthDate;
		}
	}

	private boolean isValidBirthDate(LocalDate birthDate) {	
		return birthDate.isBefore(LocalDate.now().plusYears(-16));
	}

	public void setRegisteredDate(LocalDate registeredDate) {
		assert registeredDate != null;

		if (isValidRegisteredDate(registeredDate)) {
			this.registeredDate = registeredDate;
		}
	}

	private boolean isValidRegisteredDate(LocalDate registeredDate) {	
		return !registeredDate.isAfter(LocalDate.now());
	}
	
	public void setPassword(String password) {
		assert password != null;

		if (isValidPassword(password)) {
			this.password = password;
		}
	}

	private boolean isValidPassword(String password) {
		return !password.matches("[ ]*");  		// Que no sea en blanco. Regex: [ ]+
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public User clone() {
		return  new User(this);
	}
		
	@Override
	public String toString() {
		return String.format(
				"%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s",
						"nif:", nif, 
						"name:", name, 
						"surnames:", surnames, 
						"address:", address, 
						"mail:", mail, 
						"birthDate:", birthDate, 
						"registeredDate:", registeredDate, 
						"password:", password, 
						"role:", role
				);
	}


}